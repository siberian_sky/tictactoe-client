package net.rphx.ttt.client.connectivity.impl;

import android.arch.lifecycle.LiveData;
import android.os.Handler;
import android.text.TextUtils;

import com.appspot.jbtttapp.ttt.model.FBBoard;
import com.appspot.jbtttapp.ttt.model.FBMove;
import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;

import net.rphx.ttt.client.connectivity.AsyncLiveData;
import net.rphx.ttt.client.connectivity.AsyncLoader;
import net.rphx.ttt.client.connectivity.LiveDataUpdater;
import net.rphx.ttt.client.connectivity.OnCompletionListener;
import net.rphx.ttt.client.connectivity.WebService;
import net.rphx.ttt.client.connectivity.WebserviceListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class GameService implements WebserviceListener {
    private final Handler handler = new Handler();
    private final Executor executor = Executors.newFixedThreadPool(2);

    private WebService webService;
    private AsyncLiveData<FBPlayer> playerLiveData;
    private AsyncLiveData<List<FbRoom>> roomsLiveData;
    private AsyncLiveData<FBBoard> boardLiveData;
    private FBPlayer newPlayerBeingCreated;
    private FbRoom roomInProgress;

    public void initialize(WebService webService) {
        this.webService = webService;
        webService.addListener(this);
    }

    public void close() {
        webService.removeListener(this);
        webService.close();
    }

    public boolean createRoom(String roomName, OnCompletionListener<FbRoom> listener) {
        if (roomInProgress != null) {
            return false;
        }
        FbRoom room = new FbRoom();
        room.setName(roomName);
        roomInProgress = room;
        getAsyncLoader(() -> webService.createRoom(room))
                .load(new OnCompletionListener<FbRoom>() {
                    @Override
                    public void onComplete(FbRoom data) {
                        roomInProgress = null;
                        appendToCachedList(data);
                        listener.onComplete(data);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        roomInProgress = null;
                        listener.onFailure(e);
                    }
                });
        return true;
    }

    public boolean enterRoom(String roomId, OnCompletionListener<FbRoom> listener) {
        if (roomInProgress != null) {
            return false;
        }
        FbRoom room = new FbRoom();
        room.setId(roomId);
        roomInProgress = room;
        getAsyncLoader(() -> webService.enterRoom(room))
                .load(new OnCompletionListener<FbRoom>() {
                    @Override
                    public void onComplete(FbRoom data) {
                        roomInProgress = null;
                        listener.onComplete(data);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        roomInProgress = null;
                        listener.onFailure(e);
                    }
                });
        return true;
    }

    public void createPlayer(String name, OnCompletionListener<FBPlayer> listener) {
        if (newPlayerBeingCreated != null) {
            listener.onFailure(new IllegalStateException("Call in progress for " + newPlayerBeingCreated));
        }

        FBPlayer player = new FBPlayer();
        player.setName(name);
        newPlayerBeingCreated = player;
        getDataUpdater(() -> webService.createPlayer(player), getOrCreatePlayer())
                .load(new OnCompletionListener<FBPlayer>() {
                    @Override
                    public void onComplete(FBPlayer data) {
                        newPlayerBeingCreated = null;
                        listener.onComplete(data);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        newPlayerBeingCreated = null;
                        listener.onFailure(e);
                    }
                });
    }


    public void addMove(FBMove move, OnCompletionListener<FbRoom> listener) {
        getAsyncLoader(() -> webService.addMove(move))
                .load(new OnCompletionListener<FbRoom>() {
                    @Override
                    public void onComplete(FbRoom data) {
                        newPlayerBeingCreated = null;
                        listener.onComplete(data);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        newPlayerBeingCreated = null;
                        listener.onFailure(e);
                    }
                });
    }

    public LiveData<FBPlayer> getPlayer() {
        return getOrCreatePlayer().getLiveData();
    }

    public LiveData<List<FbRoom>> getRooms() {
        return getRoomListLiveData().getLiveData();
    }

    public LiveData<FBBoard> getBoard(FbRoom room) {
        boardLiveData = getAsyncData(() -> webService.getBoard(room));
        return boardLiveData.getLiveData();
    }

    private AsyncLiveData<List<FbRoom>> getRoomListLiveData() {
        if (roomsLiveData == null) {
            roomsLiveData = getAsyncData(() -> webService.getRooms());
        }
        return roomsLiveData;
    }


    private AsyncLiveData<FBPlayer> getOrCreatePlayer() {
        if (playerLiveData == null) {
            playerLiveData = getAsyncData(() -> webService.getPlayer());
        }
        return playerLiveData;
    }

    private void appendToCachedList(FbRoom data) {
        List<FbRoom> newValue = new ArrayList<>();
        List<FbRoom> value = getRooms().getValue();
        if (value != null) {
            newValue.addAll(value);
        }
        newValue.add(data);
        getRoomListLiveData().replaceValue(newValue);
    }

    private <R> AsyncLiveData<R> getAsyncData(Callable<R> callable) {
        return new AsyncLiveData<R>(executor, handler) {
            @Override
            protected R compute() {
                try {
                    return callable.call();
                } catch (Exception ignored) {
                    return null;
                }
            }
        };
    }

    private <R> LiveDataUpdater<R> getDataUpdater(Callable<R> callable, AsyncLiveData<R> spy) {
        return new LiveDataUpdater<R>(executor, handler, spy) {
            @Override
            protected R compute() throws Exception {
                return callable.call();
            }
        };
    }

    private <R> AsyncLoader<R> getAsyncLoader(Callable<R> callable) {
        return new AsyncLoader<R>(executor, handler) {
            @Override
            protected R compute() throws Exception {
                return callable.call();
            }
        };
    }

    @Override
    public void onRoomChanged(String roomId) {
        getRoomListLiveData().invalidate();
    }

    @Override
    public void onBoardChanged(String roomId) {
        if (boardLiveData != null && boardLiveData.getLiveData().getValue() != null) {
            FBBoard value = boardLiveData.getLiveData().getValue();
            if (TextUtils.equals(value.getRoomId(), roomId)) {
                boardLiveData.invalidate();
            }
        }
    }
}
