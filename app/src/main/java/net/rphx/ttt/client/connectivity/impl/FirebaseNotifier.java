package net.rphx.ttt.client.connectivity.impl;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.rphx.ttt.client.connectivity.WebserviceListener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class FirebaseNotifier {
    private final List<WebserviceListener> listeners = new CopyOnWriteArrayList<>();
    private final ChildEventListener roomUpdateListener = new UpdateListener(this::notifyRoomChanged);
    private final ChildEventListener boardUpdateListener = new UpdateListener(this::notifyBoardChanged);

    public FirebaseNotifier() {
        getRooms().addChildEventListener(roomUpdateListener);
        getBoards().addChildEventListener(boardUpdateListener);
    }

    public void close() {
        getRooms().removeEventListener(roomUpdateListener);
        getBoards().removeEventListener(boardUpdateListener);
    }

    public void addListener(WebserviceListener listener) {
        listeners.add(listener);
    }

    public void removeListener(WebserviceListener listener) {
        listeners.remove(listener);
    }

    private void notifyRoomChanged(String roomId) {
        listeners.forEach(listener -> listener.onRoomChanged(roomId));
    }

    private void notifyBoardChanged(String roomId) {
        listeners.forEach(listener -> listener.onBoardChanged(roomId));
    }

    private DatabaseReference getRooms() {
        return FirebaseDatabase.getInstance()
                .getReference("/rooms");
    }

    private DatabaseReference getBoards() {
        return FirebaseDatabase.getInstance()
                .getReference("/boards");
    }

    private static class UpdateListener implements ChildEventListener {
        private final Consumer<String> changeConsumer;

        public UpdateListener(Consumer<String> changeConsumer) {
            this.changeConsumer = changeConsumer;
        }

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            changeConsumer.accept(dataSnapshot.getKey());
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    }
}
