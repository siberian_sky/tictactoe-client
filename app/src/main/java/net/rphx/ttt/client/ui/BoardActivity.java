package net.rphx.ttt.client.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.jbtttapp.ttt.model.FbRoom;

import net.rphx.ttt.client.R;
import net.rphx.ttt.client.components.viewmodels.BoardViewModel;
import net.rphx.ttt.client.connectivity.OnCompletionListener;
import net.rphx.ttt.client.connectivity.OnCompletionListenerTemplate;
import net.rphx.ttt.client.ui.game.GameBoard;
import net.rphx.ttt.client.ui.game.GameProcess;
import net.rphx.ttt.client.ui.view.Sign;
import net.rphx.ttt.client.ui.view.TicTacToeView;
import net.rphx.ttt.client.ui.view.WinPosition;

public class BoardActivity extends AppCompatActivity {
    public static final String PARAM_ROOM_ID = "roomId";

    private BoardViewModel gameViewModel;
    private TicTacToeView ticTacToeView;
    private TextView nextMoveView;
    private TextView nextMoveTitleView;
    private TextView waitView;
    private View contentView;
    private View emptyView;
    private boolean boardShown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        gameViewModel = ViewModelProviders.of(this).get(BoardViewModel.class);
        gameViewModel.init();

        contentView = findViewById(R.id.content);
        emptyView = findViewById(android.R.id.empty);
        nextMoveView = findViewById(R.id.next_move);
        nextMoveTitleView = findViewById(R.id.next_title);
        ticTacToeView = createBoard();
        waitView = findViewById(R.id.status);

        initData(getIntent().getStringExtra(PARAM_ROOM_ID));
    }

    private void initData(String roomId) {
        if (!gameViewModel.checkAuthenticated()) {
            return;
        }
        gameViewModel.getRoom().observe(this, this::updateRoom);
        if (!TextUtils.isEmpty(roomId)) {
            FbRoom room = gameViewModel.findRoom(roomId);
            if (room != null) {
                setTitle(room.getName());
                if (gameViewModel.checkAlreadyInRoom(room)) {
                    onRoomOpened(room);
                } else {
                    enterRoom(roomId);
                }
            } else {
                Toast.makeText(this, "Couldn't find room", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            setTitle("Creating new room...");
            createRoom();
        }
    }

    private void updateRoom(FbRoom room) {
        if (gameViewModel.getGameProcess().canMove()) {
            boolean lastMoveMine = gameViewModel.isLastMoveMine(room);
            setCanMove(!lastMoveMine);
            showWaitTurnText(lastMoveMine);
        } else {
            setCanMove(false);
            showGameFinished(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!gameViewModel.checkAuthenticated()) {
            finish();
        }
    }

    private void showContent(boolean shown, boolean animated) {
        if (animated) {
            UiUtils.setVisibilityAnimated(contentView, shown);
            UiUtils.setVisibilityAnimated(emptyView, !shown);
        } else {
            UiUtils.setVisibility(contentView, shown);
            UiUtils.setVisibility(emptyView, !shown);
        }
    }

    private TicTacToeView createBoard() {
        TicTacToeView ticTacToeView = findViewById(R.id.board);
        ticTacToeView.setOnBoardInteractionListener(new TicTacToeView.OnBoardInteractionListener() {
            @Override
            public void onBoardClick(TicTacToeView board, int row, int column) {
                Sign sign = gameViewModel.handleBoardClicked(row, column);
                if (sign != Sign.NONE) {
                    showNextMove(sign);
                    board.addMoveAnimated(sign, row, column);
                }
            }

            @Override
            public void onSignAdded(Sign sign, int row, int column) {
                sendMove(sign, row, column);
            }
        });
        return ticTacToeView;
    }

    private void onRoomOpened(FbRoom room) {
        setTitle(room.getName());
        gameViewModel.getBoard().observe(this, fbBoard -> {
            if (!boardShown) {
                ticTacToeView.setGridSize(gameViewModel.getGameProcess().getBoard().size());
                showContent(true, true);
                boardShown = true;
                updateRoom(room);
            }
            onBoardUpdated(gameViewModel.getGameProcess());
        });
    }

    private void enterRoom(String roomId) {
        gameViewModel.enterRoom(roomId, new OnCompletionListener<FbRoom>() {
            @Override
            public void onComplete(FbRoom data) {
                onRoomOpened(data);
            }

            @Override
            public void onFailure(Exception e) {
                handleRoomError();
            }
        });
    }

    private void createRoom() {
        gameViewModel.createRoom(new OnCompletionListener<FbRoom>() {
            @Override
            public void onComplete(FbRoom data) {
                onRoomOpened(data);
            }

            @Override
            public void onFailure(Exception e) {
                handleRoomError();
            }
        });
    }

    private void handleRoomError() {
        Toast.makeText(this, "Couldn't enter the room", Toast.LENGTH_SHORT).show();
    }

    private void sendMove(Sign sign, int row, int column) {
        setCanMove(false);
        gameViewModel.addMove(sign, row, column, new OnCompletionListenerTemplate<FbRoom>() {
            @Override
            public void onFailure(Exception e) {
                onMoveFailed(row, column, e);
            }
        });
    }

    private void onMoveFailed(int row, int column, Exception e) {
        Toast.makeText(this, "Couldn't make this move: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        ticTacToeView.rollbackMove(row, column);
    }

    private void setCanMove(boolean canMove) {
        ticTacToeView.setEnabled(canMove);
    }

    private void showNextMove(Sign nextMove) {
        nextMoveView.setText(nextMove.name());
    }

    private void showWinner(WinPosition winner) {
        nextMoveView.setText(winner.getWinner().name());
        nextMoveTitleView.setText(R.string.winner);
    }

    private void onBoardUpdated(GameProcess gameProcess) {
        fillBoard(gameProcess.getBoard());
        if (gameProcess.getBoard().isFinished()) {
            ticTacToeView.showWinLine(gameProcess.getBoard().getWinPosition());
            showWinner(gameProcess.getBoard().getWinPosition());
            showGameFinished(true);
        } else {
            showNextMove(gameProcess.getNextSign());
        }
    }

    private void fillBoard(GameBoard gameBoard) {
        for (int i = 0; i < gameBoard.size(); i++) {
            for (int j = 0; j < gameBoard.size(); j++) {
                Sign sign = gameBoard.getSign(i, j);
                if (sign != Sign.NONE) {
                    ticTacToeView.addMove(sign, i, j);
                }
            }
        }
    }

    private void showGameFinished(boolean show) {
        showWaitTurnText(show);
        waitView.setText(R.string.game_over);
    }

    private void showWaitTurnText(boolean show) {
        waitView.setText(R.string.wait_turn);
        if (waitView.getVisibility() == View.VISIBLE && show || waitView.getVisibility() == View.INVISIBLE && !show) {
            return;
        }
        UiUtils.setVisibilityAnimated(waitView, show);
    }
}
