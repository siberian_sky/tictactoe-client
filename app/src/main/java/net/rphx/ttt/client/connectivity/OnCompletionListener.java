package net.rphx.ttt.client.connectivity;

public interface OnCompletionListener<T> {
    void onComplete(T data);

    void onFailure(Exception e);
}
