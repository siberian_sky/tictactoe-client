package net.rphx.ttt.client.ui.game;

import net.rphx.ttt.client.ui.view.Sign;

public class GameProcess {
    private GameBoard board;
    private Sign nextSign = Sign.X;

    public void updateBoard(GameBoard board, Sign next) {
        this.board = board;
        nextSign = next;
    }

    public Sign click(int row, int col) {
        //don not process click:
        if (board.getSign(row, col) != Sign.NONE) {
            return Sign.NONE;
        }
        return nextSign;
    }

    public GameBoard getBoard() {
        return board;
    }

    public Sign getNextSign() {
        return nextSign;
    }

    public boolean canMove() {
        return board != null && !board.isFinished();
    }

}

