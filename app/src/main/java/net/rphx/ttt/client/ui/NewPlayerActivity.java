package net.rphx.ttt.client.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.appspot.jbtttapp.ttt.model.FBPlayer;

import net.rphx.ttt.client.R;
import net.rphx.ttt.client.components.viewmodels.NewPlayerViewModel;
import net.rphx.ttt.client.connectivity.OnCompletionListener;

public class NewPlayerActivity extends AppCompatActivity {

    private EditText text;
    private TextWatcher watcher;
    private View createPlayerBtn;
    private NewPlayerViewModel newPlayerViewModel;
    private TextInputLayout textInputLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_player);

        text = findViewById(R.id.player_name);
        textInputLayout = findViewById(R.id.player_input_layout);
        watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                createPlayerBtn.setEnabled(!TextUtils.isEmpty(s));
            }
        };
        text.addTextChangedListener(watcher);

        createPlayerBtn = findViewById(R.id.authenticate);
        createPlayerBtn.setOnClickListener(v -> createNewPlayer(text.getText().toString()));
        createPlayerBtn.setEnabled(false);

        newPlayerViewModel = ViewModelProviders.of(this).get(NewPlayerViewModel.class);
        newPlayerViewModel.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!newPlayerViewModel.checkAuthenticated()) {
            backToAuthentication();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        text.removeTextChangedListener(watcher);
    }

    private void createNewPlayer(String text) {
        textInputLayout.setError(null);
        newPlayerViewModel.createPlayer(text, new OnCompletionListener<FBPlayer>() {
            @Override
            public void onComplete(FBPlayer data) {
                onPlayerCreated();
            }

            @Override
            public void onFailure(Exception e) {
                textInputLayout.setError(e.getMessage());
            }
        });
    }

    private void onPlayerCreated() {
        Intent intent = new Intent(this, RoomListActivity.class);
        ActivityCompat.startActivity(this, intent, null);
        finish();
    }

    private void backToAuthentication() {
        Intent intent = new Intent(this, AuthActivity.class);
        ActivityCompat.startActivity(this, intent, null);
        finish();
    }

}
