package net.rphx.ttt.client.connectivity.impl;

import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;

import java.io.IOException;

import static java.util.Collections.singletonList;

public class TokenRequestInitializer implements HttpRequestInitializer {

    private final String authorizationToken;

    public TokenRequestInitializer(String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }

    @Override
    public void initialize(HttpRequest request) throws IOException {
        HttpHeaders headers = request.getHeaders();
        if (headers == null) {
            headers = new HttpHeaders();
            request.setHeaders(headers);
        }
        headers.put("Authorization", singletonList("Bearer " + authorizationToken));
    }
}
