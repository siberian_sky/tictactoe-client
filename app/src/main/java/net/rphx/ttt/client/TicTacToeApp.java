package net.rphx.ttt.client;

import android.app.Application;
import android.content.Context;

public class TicTacToeApp extends Application {

    private GameContext gameContext;

    @Override
    public void onCreate() {
        super.onCreate();
        gameContext = new GameContext(this);
    }

    public GameContext getGameContext() {
        return gameContext;
    }

    public static TicTacToeApp get(Context c) {
        if (c instanceof TicTacToeApp) {
            return (TicTacToeApp) c;
        } else {
            return (TicTacToeApp) c.getApplicationContext();
        }
    }
}
