package net.rphx.ttt.client.components.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import net.rphx.ttt.client.GameContext;
import net.rphx.ttt.client.TicTacToeApp;
import net.rphx.ttt.client.connectivity.impl.GameService;

public abstract class BaseAuthenticatedViewModel extends AndroidViewModel {
    private final GameContext gameContext;

    protected BaseAuthenticatedViewModel(@NonNull Application application) {
        super(application);
        gameContext = TicTacToeApp.get(application).getGameContext();
    }

    public void init() {
    }

    public boolean checkAuthenticated() {
        return gameContext.isAuthenticated();
    }

    protected GameContext getGameContext() {
        return gameContext;
    }

    protected GameService getGameService() {
        return gameContext.getGameService();
    }
}
