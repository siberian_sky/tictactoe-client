package net.rphx.ttt.client.ui.view;

public enum Sign {
    NONE, X, O
}
