package net.rphx.ttt.client.components;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.text.TextUtils;

import net.rphx.ttt.client.authentication.AuthInfo;
import net.rphx.ttt.client.authentication.FirebaseAuthentication;

public class UserRepository {
    private static final UserRepository INSTANCE = new UserRepository();
    private final FirebaseAuthentication firebaseAuthentication;
    private MutableLiveData<AuthInfo> authInfo;

    public static UserRepository getInstance() {
        return INSTANCE;
    }

    public UserRepository() {
        firebaseAuthentication = new FirebaseAuthentication();
    }

    private void retrieveAuth() {
        String userToken = firebaseAuthentication.getUserToken();
        if (TextUtils.isEmpty(userToken)) {
            authInfo.setValue(null);
            firebaseAuthentication.authenticate(uid -> {
                if (TextUtils.isEmpty(uid)) {
                    authInfo.postValue(new AuthInfo(null, null));
                } else {
                    retrieveToken(uid);
                }
            });
        } else {
            retrieveToken(userToken);
        }
    }

    private void retrieveToken(String uid) {
        firebaseAuthentication.retrieveAuthToken(token -> {
            if (TextUtils.isEmpty(token)) {
                authInfo.postValue(new AuthInfo(uid, null));
            } else {
                authInfo.postValue(new AuthInfo(uid, token));
            }
        });
    }

    public LiveData<AuthInfo> getAuthInfo() {
        if (authInfo == null) {
            authInfo = new MutableLiveData<>();
            retrieveAuth();
        }
        return authInfo;
    }
}
