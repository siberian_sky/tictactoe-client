package net.rphx.ttt.client.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;

import net.rphx.ttt.client.R;
import net.rphx.ttt.client.components.viewmodels.RoomListViewModel;
import net.rphx.ttt.client.ui.data.ListData;
import net.rphx.ttt.client.ui.data.RoomListAdapter;
import net.rphx.ttt.client.ui.view.EmptyRecyclerView;

import java.util.List;

public class RoomListActivity extends AppCompatActivity {
    private RoomListViewModel roomListViewModel;
    private RoomListAdapter adapter;
    private FloatingActionButton fab;
    private boolean fabShown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        roomListViewModel = ViewModelProviders.of(this).get(RoomListViewModel.class);
        roomListViewModel.init();

        EmptyRecyclerView recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        TextView emptyView = findViewById(android.R.id.empty);
        emptyView.setText(R.string.loading_rooms);
        recyclerView.setEmptyView(emptyView);

        adapter = new RoomListAdapter(this, dataContainer);
        adapter.setItemClickListener((view, position, viewHolder) -> {
            FbRoom room = adapter.getItem(position);
            showRoom(room, view);
        });
        recyclerView.setAdapter(adapter);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> showRoom(null, v));

        subscribeToData(emptyView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.notifyDataSetChanged();
        if (!roomListViewModel.checkAuthenticated()) {
            backToAuthentication();
        }
    }

    private void subscribeToData(TextView emptyView) {
        FBPlayer player = roomListViewModel.getPlayer().getValue();
        if (player != null) {
            setTitle(player.getName());
        }
        roomListViewModel.getRooms().observe(this, rooms -> onRoomsUpdated(emptyView, rooms));
    }

    private final ListData<FbRoom> dataContainer = new ListData<FbRoom>() {
        @Override
        public FbRoom get(int index) {
            //noinspection ConstantConditions
            return roomListViewModel.getRooms().getValue().get(index);
        }

        @Override
        public int size() {
            List<FbRoom> rooms = roomListViewModel.getRooms().getValue();
            return rooms == null ? 0 : rooms.size();
        }
    };

    private void onRoomsUpdated(TextView emptyView, List<FbRoom> rooms) {
        if (!fabShown) {
            fab.show();
            if (rooms != null && rooms.isEmpty()) {
                emptyView.setText(R.string.no_rooms_found);
            }
            fabShown = true;
        }
        adapter.notifyDataSetChanged();
    }

    private void showRoom(FbRoom room, View source) {
        Intent intent = new Intent(this, BoardActivity.class);
        if (room != null) {
            intent.putExtra(BoardActivity.PARAM_ROOM_ID, room.getId());
        }
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeScaleUpAnimation(source, 0, 0, source.getWidth(), source.getHeight());
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    private void backToAuthentication() {
        Intent intent = new Intent(this, AuthActivity.class);
        ActivityCompat.startActivity(this, intent, null);
        finish();
    }
}
