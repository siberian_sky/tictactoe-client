package net.rphx.ttt.client.connectivity;

public interface WebserviceListener {

    void onRoomChanged(String roomId);

    void onBoardChanged(String roomId);

}
