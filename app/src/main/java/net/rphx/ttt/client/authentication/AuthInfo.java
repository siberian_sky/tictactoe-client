package net.rphx.ttt.client.authentication;

public class AuthInfo {
    private final String uid;
    private final String authToken;

    public AuthInfo(String uid, String authToken) {
        this.uid = uid;
        this.authToken = authToken;
    }

    public String getUid() {
        return uid;
    }

    public String getAuthToken() {
        return authToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthInfo authInfo = (AuthInfo) o;

        if (uid != null ? !uid.equals(authInfo.uid) : authInfo.uid != null) return false;
        return authToken != null ? authToken.equals(authInfo.authToken) : authInfo.authToken == null;
    }

    @Override
    public int hashCode() {
        int result = uid != null ? uid.hashCode() : 0;
        result = 31 * result + (authToken != null ? authToken.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AuthInfo{" +
                "uid='" + uid + '\'' +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}
