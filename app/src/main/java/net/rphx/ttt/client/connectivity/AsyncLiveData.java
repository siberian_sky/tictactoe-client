package net.rphx.ttt.client.connectivity;

import android.arch.lifecycle.MutableLiveData;
import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AsyncLiveData<T> {

    private final MutableLiveData<T> mLiveData;
    private final Handler handler;
    private final Executor executor;

    private final AtomicBoolean mInvalid = new AtomicBoolean(true);
    private final AtomicBoolean mComputing = new AtomicBoolean(false);

    protected AsyncLiveData(Executor executor, Handler handler) {
        this.executor = executor;
        this.handler = handler;
        mLiveData = new MutableLiveData<T>() {
            @Override
            protected void onActive() {
                executor.execute(mRefreshRunnable);
            }
        };
    }

    public MutableLiveData<T> getLiveData() {
        return mLiveData;
    }

    final Runnable mRefreshRunnable = new Runnable() {
        @WorkerThread
        @Override
        public void run() {
            boolean computed;
            do {
                computed = false;
                // compute can happen only in 1 thread but no reason to lock others.
                if (mComputing.compareAndSet(false, true)) {
                    // as long as it is invalid, keep computing.
                    try {
                        T value = null;
                        while (mInvalid.compareAndSet(true, false)) {
                            computed = true;
                            value = compute();
                        }
                        if (computed) {
                            mLiveData.postValue(value);
                        }
                    } finally {
                        // release compute lock
                        mComputing.set(false);
                    }
                }
                // check invalid after releasing compute lock to avoid the following scenario.
                // Thread A runs compute()
                // Thread A checks invalid, it is false
                // Main thread sets invalid to true
                // Thread B runs, fails to acquire compute lock and skips
                // Thread A releases compute lock
                // We've left invalid in set state. The check below recovers.
            } while (computed && mInvalid.get());
        }
    };

    @WorkerThread
    public void replaceValue(T value) {
        mLiveData.postValue(value);
    }

    final Runnable mInvalidationRunnable = new Runnable() {
        @MainThread
        @Override
        public void run() {
            boolean isActive = mLiveData.hasActiveObservers();
            if (mInvalid.compareAndSet(false, true)) {
                if (isActive) {
                    executor.execute(mRefreshRunnable);
                }
            }
        }
    };

    public void invalidate() {
        handler.post(mInvalidationRunnable);
    }

    @WorkerThread
    protected abstract T compute();
}
