package net.rphx.ttt.client.connectivity.impl;

import com.appspot.jbtttapp.ttt.Ttt;
import com.appspot.jbtttapp.ttt.model.FBBoard;
import com.appspot.jbtttapp.ttt.model.FBMove;
import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.json.gson.GsonFactory;

import net.rphx.ttt.client.connectivity.WebService;
import net.rphx.ttt.client.connectivity.WebserviceListener;

import java.io.IOException;
import java.util.List;

public class WebServiceImpl implements WebService {
    private final Ttt service;
    private final FirebaseNotifier notifier;

    public WebServiceImpl(String serverUrl, String userAuthInfo) {
        Ttt.Builder builder = new Ttt.Builder(
                AndroidHttp.newCompatibleTransport(),
                new GsonFactory(),
                new TokenRequestInitializer(userAuthInfo));
        builder.setRootUrl(serverUrl);
        service = builder.build();
        notifier = new FirebaseNotifier();
    }

    @Override
    public FbRoom createRoom(FbRoom room) throws IOException {
        return unpackException(() -> service.rooms().add(room).execute());
    }

    @Override
    public FbRoom enterRoom(FbRoom room) throws IOException {
        return unpackException(() -> service.rooms().enter(room).execute());
    }

    @Override
    public FBPlayer createPlayer(FBPlayer player) throws IOException {
        return unpackException(() -> service.players().create(player).execute());
    }

    @Override
    public FBPlayer getPlayer() throws IOException {
        return unpackException(() -> service.players().get().execute());
    }

    @Override
    public FbRoom addMove(FBMove move) throws IOException {
        return unpackException(() -> service.board().move(move).execute());
    }

    @Override
    public FBBoard getBoard(FbRoom room) throws IOException {
        return unpackException(() -> service.board().get(room).execute());
    }

    @Override
    public List<FbRoom> getRooms() throws IOException {
        return unpackException(() -> service.rooms().get().execute().getItems());
    }

    @Override
    public void addListener(WebserviceListener listener) {
        notifier.addListener(listener);
    }

    @Override
    public void removeListener(WebserviceListener listener) {
        notifier.removeListener(listener);
    }

    @Override
    public void close() {
        notifier.close();
    }

    private <V> V unpackException(IOCallable<V> runnable) throws IOException {
        try {
            return runnable.call();
        } catch (GoogleJsonResponseException e) {
            String message = e.getDetails().getMessage();
            int i = message.indexOf("Exception:");
            if (i != -1) {
                message = message.substring(i + "Exception:".length()).trim();
            }
            throw new IOException(message, e);
        }
    }

    @SuppressWarnings("InterfaceNamingConvention")
    private interface IOCallable<V> {
        V call() throws IOException;

    }
}
