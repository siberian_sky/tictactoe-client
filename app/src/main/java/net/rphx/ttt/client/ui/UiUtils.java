package net.rphx.ttt.client.ui;

import android.animation.Animator;
import android.view.View;

public class UiUtils {

    private UiUtils() {
    }

    public static void setVisibilityAnimated(View view, boolean show) {
        view.animate()
                .alpha(show ? 1f : 0f)
                .setDuration(view.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime))
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        view.setVisibility(show ? View.GONE : View.VISIBLE);
                        view.setAlpha(show ? 0f : 1f);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(show ? View.VISIBLE : View.GONE);
                        view.setAlpha(show ? 1f : 0f);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

    public static void setVisibility(View view, boolean show) {
        view.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
