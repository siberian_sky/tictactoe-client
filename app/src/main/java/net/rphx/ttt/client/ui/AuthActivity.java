package net.rphx.ttt.client.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import net.rphx.ttt.client.R;
import net.rphx.ttt.client.authentication.AuthInfo;
import net.rphx.ttt.client.components.viewmodels.AuthenticationViewModel;
import net.rphx.ttt.client.components.viewmodels.PlayerAuthenticationViewModel;

public class AuthActivity extends AppCompatActivity {
    private TextView authStatusView;
    private PlayerAuthenticationViewModel playerModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        authStatusView = findViewById(R.id.status);

        AuthenticationViewModel authViewModel =
                ViewModelProviders.of(this).get(AuthenticationViewModel.class);
        authViewModel.init();
        authViewModel.getAuthInfo().observe(this, this::onAuthenticationChanged);

        playerModel = ViewModelProviders.of(this).get(PlayerAuthenticationViewModel.class);
    }

    private void onAuthenticationChanged(AuthInfo authInfo) {
        if (authInfo == null) {
            //logging in
            onNotAuthenticated();
            return;
        }
        if (TextUtils.isEmpty(authInfo.getUid()) || TextUtils.isEmpty(authInfo.getAuthToken())) {
            //Couldn't authenticate
            onAuthenticationError();
        } else {
            onAuthenticated(authInfo);
        }
    }

    private void onAuthenticated(AuthInfo authInfo) {
        authStatusView.setText(R.string.on_authenticated);
        playerModel.init(authInfo);
        playerModel.getPlayer().observe(this, player -> {
            if (player == null) {
                onProfileLoadError();
            } else if (TextUtils.isEmpty(player.getId())) {
                //does not exist
                createNewPlayer();
            } else {
                onPlayerLoaded();
            }
        });
    }

    private void onNotAuthenticated() {
        authStatusView.setAlpha(0);
        authStatusView.setText(R.string.logging_in);
        authStatusView.animate()
                .alpha(1f)
                .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .setListener(null);
    }

    private void onAuthenticationError() {
        authStatusView.setText(R.string.auth_error);
    }

    private void onProfileLoadError() {
        authStatusView.setText(R.string.profile_load_error);
    }

    private void createNewPlayer() {
        Intent intent = new Intent(this, NewPlayerActivity.class);
        ActivityCompat.startActivity(this, intent, null);
        finish();
    }

    private void onPlayerLoaded() {
        Intent intent = new Intent(this, RoomListActivity.class);
        ActivityCompat.startActivity(this, intent, null);
        finish();
    }
}
