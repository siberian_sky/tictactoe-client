package net.rphx.ttt.client.components.viewmodels;

import android.app.Application;
import android.support.annotation.NonNull;

import com.appspot.jbtttapp.ttt.model.FBPlayer;

import net.rphx.ttt.client.connectivity.OnCompletionListener;

public class NewPlayerViewModel extends BaseAuthenticatedViewModel {

    public NewPlayerViewModel(@NonNull Application application) {
        super(application);
    }

    public void createPlayer(String name, OnCompletionListener<FBPlayer> listener) {
        getGameContext().getGameService().createPlayer(name, listener);
    }
}
