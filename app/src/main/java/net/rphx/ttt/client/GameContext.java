package net.rphx.ttt.client;

import android.content.Context;

import net.rphx.ttt.client.authentication.AuthInfo;
import net.rphx.ttt.client.connectivity.impl.GameService;
import net.rphx.ttt.client.connectivity.impl.WebServiceImpl;

public class GameContext {
    private GameService gameService;
    private AuthInfo authInfo;
    private final String serverUrl;

    public GameContext(Context context) {
        serverUrl = context.getResources().getString(R.string.prod_url);
    }

    public void init(AuthInfo authInfo) {
        if (authInfo.equals(this.authInfo)) {
            return;
        }
        this.authInfo = authInfo;

        if (gameService != null) {
            gameService.close();
        }
        gameService = new GameService();
        gameService.initialize(new WebServiceImpl(serverUrl, authInfo.getAuthToken()));
    }

    public boolean isAuthenticated() {
        return authInfo != null;
    }

    public GameService getGameService() {
        return gameService;
    }
}
