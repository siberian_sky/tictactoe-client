package net.rphx.ttt.client.connectivity;

import android.os.Handler;
import android.support.annotation.WorkerThread;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AsyncLoader<T> {
    private final Executor executor;
    private final Handler handler;
    private final AtomicBoolean mComputing = new AtomicBoolean(false);

    protected AsyncLoader(Executor executor, Handler handler) {
        this.executor = executor;
        this.handler = handler;
    }

    public void load(OnCompletionListener<T> listener) {
        executor.execute(() -> refreshValue(listener));
    }

    private void refreshValue(OnCompletionListener<T> listener) {
        if (mComputing.compareAndSet(false, true)) {
            try {
                T compute = compute();
                onValueComputed(compute);
                handler.post(() -> listener.onComplete(compute));
            } catch (Exception e) {
                handler.post(() -> listener.onFailure(e));
            }
        }
    }

    protected void onValueComputed(T compute) {
    }

    @WorkerThread
    protected abstract T compute() throws Exception;
}
