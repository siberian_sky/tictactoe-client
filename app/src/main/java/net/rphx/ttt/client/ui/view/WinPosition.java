package net.rphx.ttt.client.ui.view;

public class WinPosition {

    public static final WinPosition NONE = new WinPosition(Sign.NONE, 0, Type.ROW);
    private final Sign winner;
    private final int value;
    private final Type type;

    public WinPosition(Sign winner, int value, Type type) {
        this.winner = winner;
        this.value = value;
        this.type = type;
    }

    public enum Type {
        ROW, COL, D1, D2
    }

    public Sign getWinner() {
        return winner;
    }

    public int getValue() {
        return value;
    }

    public Type getType() {
        return type;
    }
}
