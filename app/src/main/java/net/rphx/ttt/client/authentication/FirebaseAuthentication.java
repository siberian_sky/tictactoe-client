package net.rphx.ttt.client.authentication;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import java.util.function.Consumer;

public class FirebaseAuthentication {

    private final FirebaseAuth mAuth;

    public FirebaseAuthentication() {
        mAuth = FirebaseAuth.getInstance();
    }

    public String getUserToken() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        return (currentUser != null) ? currentUser.getUid() : null;
    }

    public void retrieveAuthToken(final Consumer<String> result) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            result.accept(null);
            return;
        }
        Task<GetTokenResult> idToken = currentUser.getIdToken(false);
        idToken.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                result.accept(task.getResult().getToken());
            } else {
                result.accept(null);
            }
        });
    }

    public void authenticate(final Consumer<String> result) {
        mAuth.signInAnonymously().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                result.accept(getUserToken());
            } else {
                result.accept(null);
            }
        });
    }
}
