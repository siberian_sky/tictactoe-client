package net.rphx.ttt.client.components.viewmodels;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;

import java.util.List;

public class RoomListViewModel extends BaseAuthenticatedViewModel {

    public RoomListViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<FbRoom>> getRooms() {
        return getGameService().getRooms();
    }

    public LiveData<FBPlayer> getPlayer() {
        return getGameService().getPlayer();
    }

}
