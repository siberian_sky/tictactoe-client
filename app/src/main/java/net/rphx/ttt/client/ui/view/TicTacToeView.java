package net.rphx.ttt.client.ui.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import net.rphx.ttt.client.R;

import java.util.ArrayList;
import java.util.List;

public class TicTacToeView extends View implements GestureDetector.OnGestureListener, ValueAnimator.AnimatorUpdateListener, Animator.AnimatorListener {
    public static final int DEFAULT_GRID_SIZE = 3;

    private Paint tintPaint;
    private Paint gridPaint;
    private Paint signPaint;
    private Paint winLinePaint;

    private GestureDetector gestureDetector;
    private OnBoardInteractionListener onBoardInteractionListener;

    private ValueAnimator clickAnimator;
    private ValueAnimator winLineAnimator;
    private float strokeWidth;

    private float signRadius;

    private float animatedSignRadius;
    private float winLineLength;

    private int gridSize = DEFAULT_GRID_SIZE;
    private float[] gridLinePoints;
    private PointF[][] centerPoints;

    private final List<CellData> cells = new ArrayList<>();
    private WinPosition winPosition = WinPosition.NONE;

    public TicTacToeView(Context context) {
        super(context);
        init();
    }

    public TicTacToeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TicTacToeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        strokeWidth = getResources().getDimension(R.dimen.grid_stroke_width);

        tintPaint = new Paint();
        tintPaint.setColor(getResources().getColor(R.color.board_tint_color, null));
        tintPaint.setStyle(Paint.Style.FILL);

        gridPaint = new Paint();
        gridPaint.setColor(getResources().getColor(R.color.grid_color, null));
        gridPaint.setAntiAlias(true);
        gridPaint.setStrokeWidth(strokeWidth);
        gridPaint.setStrokeCap(Paint.Cap.ROUND);

        signPaint = new Paint();
        signPaint.setColor(getContext().getResources().getColor(R.color.sign_color, null));
        signPaint.setAntiAlias(true);
        signPaint.setStyle(Paint.Style.STROKE);
        signPaint.setStrokeWidth(strokeWidth);
        signPaint.setStrokeCap(Paint.Cap.ROUND);

        winLinePaint = new Paint();
        winLinePaint.setColor(getContext().getResources().getColor(R.color.win_color, null));
        winLinePaint.setAntiAlias(true);
        winLinePaint.setStrokeWidth(strokeWidth * 2);
        winLinePaint.setStrokeCap(Paint.Cap.ROUND);

        gestureDetector = new GestureDetector(getContext(), this);

        clickAnimator = new ValueAnimator();
        clickAnimator.setDuration(150);
        clickAnimator.setInterpolator(new DecelerateInterpolator());
        clickAnimator.addUpdateListener(this);
        clickAnimator.addListener(this);

        winLineAnimator = new ValueAnimator();
        winLineAnimator.setDuration(150);
        winLineAnimator.setInterpolator(new DecelerateInterpolator());
        winLineAnimator.addUpdateListener(this);
        winLineAnimator.addListener(this);

        reinitBoard();
    }

    private void reinitBoard() {
        gridLinePoints = new float[4 * 2 * (gridSize - 1)];
        centerPoints = new PointF[gridSize][gridSize];
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                centerPoints[i][j] = new PointF();
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        getLayoutParams().height = getMeasuredWidth();
        recalculateBoard();
    }

    private void recalculateBoard() {
        setGridLinePoints();
        setCenterPoints();
        setAnimationValues();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawGrid(canvas);
        super.onDraw(canvas);
        drawCells(canvas);
        drawWinLine(canvas);
        if (!isEnabled()) {
            canvas.drawRect(0, 0, getWidth(), getHeight(), tintPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled() || clickAnimator.isRunning() || isAnimationFlagSet()) {
            return super.onTouchEvent(event);
        } else {
            return gestureDetector.onTouchEvent(event);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        invalidate();
    }

    public void setGridSize(int gridSize) {
        if (gridSize < DEFAULT_GRID_SIZE) {
            throw new IllegalStateException("Min supported grid size is " + DEFAULT_GRID_SIZE);
            //what about max?
        }
        this.gridSize = gridSize;
        reinitBoard();
        recalculateBoard();
        invalidate();
    }

    public int getGridSize() {
        return gridSize;
    }

    public void setOnBoardInteractionListener(OnBoardInteractionListener onBoardInteractionListener) {
        this.onBoardInteractionListener = onBoardInteractionListener;
    }

    public boolean isOccupied(int row, int column) {
        return cells.stream()
                .anyMatch(signData ->
                        (signData.getRow() == row) && (signData.getColumn() == column));
    }

    public void showWinLine(WinPosition winPosition) {
        this.winPosition = winPosition;
        winLineAnimator.start();
    }

    public void addMoveAnimated(Sign sign, int row, int column) {
        CellData cellData = new CellData();
        cellData.setSign(sign);
        cellData.setRow(row);
        cellData.setColumn(column);
        cellData.setAnimated(true);
        if (clickAnimator.isRunning()) {
            clickAnimator.end();
        }
        cells.add(cellData);
        clickAnimator.start();
    }

    public void addMove(Sign sign, int row, int column) {
        CellData cellData = new CellData();
        cellData.setSign(sign);
        cellData.setRow(row);
        cellData.setColumn(column);
        cellData.setAnimated(false);
        if (clickAnimator.isRunning()) {
            clickAnimator.end();
        }
        cells.add(cellData);
        invalidate();
    }

    public void rollbackMove(int row, int column) {
        cells.stream()
                .filter(cell -> cell.getRow() == row && cell.getColumn() == column)
                .findAny()
                .ifPresent(cell -> {
                    if (clickAnimator.isRunning()) {
                        clickAnimator.end();
                    }
                    cells.remove(cell);
                    invalidate();
                });
    }

    private boolean isAnimationFlagSet() {
        for (CellData cellData : cells) {
            if (cellData.isAnimated()) {
                return true;
            }
        }
        return false;
    }

    private void setGridLinePoints() {
        int side = getMeasuredWidth();
        float padding = strokeWidth;
        int half = gridLinePoints.length / 2;
        for (int i = 0; i < gridSize - 1; i++) {
            /*hor*/
            gridLinePoints[4 * i] =
                    /*vert*/gridLinePoints[half + 4 * i + 1] = padding;//0
            /*hor*/
            gridLinePoints[4 * i + 1] = gridLinePoints[4 * i + 3] =
                    /*vert*/gridLinePoints[half + 4 * i] = gridLinePoints[half + 4 * i + 2] =
                    (i + 1) * side / gridSize;//1,3
             /*hor*/
            gridLinePoints[4 * i + 2] =
                    /*vert*/gridLinePoints[half + 4 * i + 3] =
                    side - padding;//2
        }
    }

    private void setCenterPoints() {
        float radius = getHalfCell();
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                centerPoints[i][j].x = radius + (j * (2 * radius));
                centerPoints[i][j].y = radius + (i * (2 * radius));
            }
        }
    }

    private float getHalfCell() {
        return 1.0f * getMeasuredWidth() / gridSize / 2;
    }

    private void setAnimationValues() {
        signRadius = getMeasuredWidth() / gridSize / 2 - 2 * strokeWidth;
        clickAnimator.setFloatValues(0, signRadius);
        winLineAnimator.setFloatValues(0, getMeasuredWidth());
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (animation == clickAnimator) {
            CellData signData = cells.get(cells.size() - 1);
            signData.setAnimated(false);
            onBoardInteractionListener.onSignAdded(signData.getSign(), signData.getRow(), signData.getColumn());
            animatedSignRadius = 0;
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        if (animation == clickAnimator) {
            animatedSignRadius = (float) animation.getAnimatedValue();
        } else if (animation == winLineAnimator) {
            winLineLength = (float) animation.getAnimatedValue();
        }
        invalidate();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        float x = e.getX();
        float y = e.getY();
        int row = detectIndexOfPartition(y);
        int column = detectIndexOfPartition(x);
        if ((row != -1) && (column != -1)) {
            onBoardInteractionListener.onBoardClick(this, row, column);
        }
        return true;
    }

    private int detectIndexOfPartition(float value) {
        float cellSize = getMeasuredWidth() / gridSize;
        return (int) (value / cellSize);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    private void drawCells(Canvas canvas) {
        for (int i = 0; i < cells.size(); i++) {
            CellData cellData = cells.get(i);
            switch (cellData.getSign()) {
                case O:
                    drawO(canvas, centerPoints[cellData.getRow()][cellData.getColumn()], cellData.isAnimated());
                    break;
                case X:
                    drawX(canvas, centerPoints[cellData.getRow()][cellData.getColumn()], cellData.isAnimated());
                    break;
                case NONE:
                    break;
            }
        }
    }

    private void drawO(Canvas canvas, PointF center, boolean animationFlag) {
        float radius = animationFlag ? animatedSignRadius : signRadius;
        canvas.drawCircle(center.x, center.y, radius, signPaint);
    }

    private void drawX(Canvas canvas, PointF center, boolean animationFlag) {
        float radius = animationFlag ? animatedSignRadius : signRadius;
        canvas.drawLine(center.x - radius, center.y - radius, center.x + radius, center.y + radius, signPaint);
        canvas.drawLine(center.x - radius, center.y + radius, center.x + radius, center.y - radius, signPaint);
    }

    private void drawGrid(Canvas canvas) {
        canvas.drawLines(gridLinePoints, gridPaint);
    }

    private void drawWinLine(Canvas canvas) {
        if (winPosition.getWinner() == Sign.NONE) {
            return;
        }
        float length = winLineLength;
        float radius = getHalfCell();
        float padding = strokeWidth;
        switch (winPosition.getType()) {
            case ROW:
                float y = radius + winPosition.getValue() * 2 * radius;
                canvas.drawLine(padding, y, length - padding, y, winLinePaint);
                break;
            case COL:
                float x = radius + winPosition.getValue() * 2 * radius;
                canvas.drawLine(x, padding, x, length - padding, winLinePaint);
                break;
            case D1:
                canvas.drawLine(padding, padding, length - padding, length - padding, winLinePaint);
                break;
            case D2:
                canvas.drawLine(getMeasuredWidth() - padding, padding, padding + getMeasuredWidth()
                        - length, length - padding, winLinePaint);
                break;
        }
    }

    public interface OnBoardInteractionListener {

        void onBoardClick(TicTacToeView board, int row, int column);

        void onSignAdded(Sign sign, int row, int column);

    }

    private static class CellData {
        private Sign sign;
        private int row;
        private int column;
        private boolean animated;

        public Sign getSign() {
            return sign;
        }

        public void setSign(Sign sign) {
            this.sign = sign;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public boolean isAnimated() {
            return animated;
        }

        public void setAnimated(boolean animated) {
            this.animated = animated;
        }
    }
}
