package net.rphx.ttt.client.ui.data;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseAdapter<I> extends RecyclerView.Adapter<BaseAdapter.RecyclerItemHolder<I>> {

    private final ListData<I> container;
    private final LayoutInflater inflater;
    protected OnRecyclerItemClickListener<I> itemClickListener;

    protected BaseAdapter(Context context, ListData<I> container) {
        this.container = container;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerItemHolder<I> onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = createView(inflater, parent, viewType);
        return createHolder(view, viewType);
    }

    protected abstract RecyclerItemHolder<I> createHolder(View view, int viewType);

    @LayoutRes
    protected abstract int getItemLayoutId(int viewType);

    protected LayoutInflater getInflater() {
        return inflater;
    }

    protected View createView(LayoutInflater inflater, ViewGroup viewGroup, int viewType) {
        return inflater.inflate(getItemLayoutId(viewType), viewGroup, false);
    }

    @Override
    public void onBindViewHolder(RecyclerItemHolder<I> holder, int position) {
        holder.setData(getItem(position), position);
    }

    public void setItemClickListener(OnRecyclerItemClickListener<I> listener) {
        itemClickListener = listener;
    }

    public I getItem(int position) {
        return container.get(position);
    }

    @Override
    public int getItemCount() {
        return container.size();
    }

    public static class RecyclerItemHolder<I> extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final OnRecyclerItemClickListener<I> itemClickListener;

        public RecyclerItemHolder(View view, OnRecyclerItemClickListener<I> itemClickListener) {
            super(view);
            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;
        }

        public void setData(I data, int position) {
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition(), this);
            }
        }
    }
}
