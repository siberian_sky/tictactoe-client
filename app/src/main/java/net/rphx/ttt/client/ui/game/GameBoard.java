package net.rphx.ttt.client.ui.game;

import net.rphx.ttt.client.ui.view.Sign;
import net.rphx.ttt.client.ui.view.WinPosition;

public class GameBoard {
    private final Sign[][] board;
    private final WinPosition winPosition;

    private GameBoard(Sign[][] board) {
        this.board = board;
        winPosition = checkWinner(board);
    }

    public int size() {
        return board.length;
    }

    public Sign getSign(int row, int column) {
        return board[row][column];
    }

    boolean isSameSize(GameBoard gameBoard) {
        return board.length == gameBoard.board.length;
    }

    public boolean isFinished() {
        return winPosition != null;
    }

    public WinPosition getWinPosition() {
        return winPosition;
    }

    public static GameBoard parse(String state) {
        int size = (int) Math.sqrt(state.length());
        Sign[][] board = new Sign[size][size];
        for (int i = 0; i < state.length(); i++) {
            board[i / size][i % size] = toSign(state.charAt(i));
        }
        return new GameBoard(board);
    }

    private static Sign toSign(char charSign) {
        switch (charSign) {
            case 'x':
                return Sign.X;
            case 'o':
                return Sign.O;
            default:
                return Sign.NONE;
        }
    }

    private static WinPosition checkWinner(Sign[][] board) {
        boolean d1Win = true;
        boolean d2Win = true;
        boolean freeCells = false;
        for (int i = 0, n = board.length; i < n; i++) {
            boolean verticalWin = true;
            boolean horizontalWin = true;
            for (int j = 0; j < n; j++) {
                if (board[i][j] == Sign.NONE || board[i][j] != board[i][0]) {
                    horizontalWin = false;
                }
                if (board[j][i] == Sign.NONE || board[j][i] != board[0][i]) {
                    verticalWin = false;
                }
                if (board[i][j] == Sign.NONE) {
                    freeCells = true;
                }
            }

            if (horizontalWin) {
                Sign sign = board[i][0];
                return new WinPosition(sign, i, WinPosition.Type.ROW);
            }
            if (verticalWin) {
                Sign sign = board[0][i];
                return new WinPosition(sign, i, WinPosition.Type.COL);
            }
            if (board[i][i] == Sign.NONE || board[i][i] != board[0][0]) {
                d1Win = false;
            }
            if (board[i][n - i - 1] == Sign.NONE || board[i][n - i - 1] != board[0][n - 1]) {
                d2Win = false;
            }
        }
        if (d1Win) {
            Sign sign = board[0][0];
            return new WinPosition(sign, 0, WinPosition.Type.D1);
        }
        if (d2Win) {
            Sign sign = board[0][board.length - 1];
            return new WinPosition(sign, 0, WinPosition.Type.D2);
        }
        return freeCells ? null : WinPosition.NONE;
    }
}
