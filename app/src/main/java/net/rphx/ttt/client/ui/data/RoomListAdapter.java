package net.rphx.ttt.client.ui.data;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appspot.jbtttapp.ttt.model.FbRoom;

import net.rphx.ttt.client.R;

public class RoomListAdapter extends BaseAdapter<FbRoom> {

    public RoomListAdapter(Context context, ListData<FbRoom> data) {
        super(context, data);
    }

    @Override
    public RecyclerItemHolder<FbRoom> onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = createView(getInflater(), parent, viewType);
        return createHolder(view, viewType);
    }

    @Override
    protected RecyclerItemHolder<FbRoom> createHolder(View view, int viewType) {
        return new RoomItemHolder(view, itemClickListener);
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        return R.layout.room_item;
    }

    protected static class RoomItemHolder extends RecyclerItemHolder<FbRoom> {

        private final TextView name;
        private final TextView lastMoveTime;
        private final TextView playerCount;

        public RoomItemHolder(View view, OnRecyclerItemClickListener<FbRoom> itemClickListener) {
            super(view, itemClickListener);
            name = view.findViewById(R.id.name);
            lastMoveTime = view.findViewById(R.id.last_played);
            playerCount = view.findViewById(R.id.player_count);
        }

        @Override
        public void setData(FbRoom data, int position) {
            super.setData(data, position);
            name.setText(data.getName());
            formatTime(data);
            playerCount.setText(itemView.getResources().getString(R.string.players_count, getSize(data)));
        }

        private void formatTime(FbRoom data) {
            Long lastMove = data.getLastMoveTime();
            if (lastMove == null || lastMove == 0) {
                lastMoveTime.setText("---");
            } else {
                lastMoveTime.setText(
                        DateUtils.getRelativeDateTimeString(lastMoveTime.getContext(), lastMove,
                                DateUtils.SECOND_IN_MILLIS,
                                DateUtils.HOUR_IN_MILLIS, 0));
            }
        }

        private int getSize(FbRoom data) {
            return data.getPlayers() != null ? data.getPlayers().size() : 0;
        }
    }
}
