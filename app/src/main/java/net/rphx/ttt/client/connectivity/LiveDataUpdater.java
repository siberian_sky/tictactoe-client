package net.rphx.ttt.client.connectivity;

import android.os.Handler;

import java.util.concurrent.Executor;

public abstract class LiveDataUpdater<T> extends AsyncLoader<T> {
    private final AsyncLiveData<T> liveData;

    protected LiveDataUpdater(Executor executor, Handler handler, AsyncLiveData<T> liveData) {
        super(executor, handler);
        this.liveData = liveData;
    }

    @Override
    protected void onValueComputed(T computed) {
        liveData.replaceValue(computed);
    }
}
