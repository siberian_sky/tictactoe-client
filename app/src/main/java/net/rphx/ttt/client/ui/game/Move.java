package net.rphx.ttt.client.ui.game;

import net.rphx.ttt.client.ui.view.Sign;

public class Move {
    private final Sign sign;
    private final int row;
    private final int col;

    public Move(Sign sign, int row, int col) {
        this.sign = sign;
        this.row = row;
        this.col = col;
    }

    public Sign getSign() {
        return sign;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Move move = (Move) o;

        if (row != move.row) {
            return false;
        }
        if (col != move.col) {
            return false;
        }
        return sign == move.sign;
    }

    @Override
    public int hashCode() {
        int result = sign.hashCode();
        result = 31 * result + row;
        result = 31 * result + col;
        return result;
    }

    @Override
    public String toString() {
        return "Move{" +
                "sign=" + sign +
                ", row=" + row +
                ", col=" + col +
                '}';
    }
}
