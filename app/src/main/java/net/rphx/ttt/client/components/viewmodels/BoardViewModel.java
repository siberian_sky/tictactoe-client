package net.rphx.ttt.client.components.viewmodels;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.appspot.jbtttapp.ttt.model.FBBoard;
import com.appspot.jbtttapp.ttt.model.FBMove;
import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;

import net.rphx.ttt.client.connectivity.OnCompletionListener;
import net.rphx.ttt.client.ui.game.GameBoard;
import net.rphx.ttt.client.ui.game.GameProcess;
import net.rphx.ttt.client.ui.view.Sign;

import java.util.List;

public class BoardViewModel extends BaseAuthenticatedViewModel {
    private final GameProcess gameProcess;
    private final MutableLiveData<FbRoom> roomLiveData = new MutableLiveData<>();
    private final MutableLiveData<FBBoard> boardLiveData = new MutableLiveData<>();
    private LiveData<FBBoard> subscribedLiveData;

    public BoardViewModel(@NonNull Application application) {
        super(application);
        gameProcess = new GameProcess();
        getRooms().observeForever(roomsObserver);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        getRooms().removeObserver(roomsObserver);
        clearBoardObserver();
    }

    public LiveData<FbRoom> getRoom() {
        return roomLiveData;
    }

    public LiveData<FBBoard> getBoard() {
        return boardLiveData;
    }

    public FbRoom findRoom(String roomId) {
        List<FbRoom> value = getRooms().getValue();
        return value == null ? null :
                value.stream()
                        .filter(room -> room.getId().equals(roomId))
                        .findAny()
                        .orElse(null);
    }

    public boolean checkAlreadyInRoom(FbRoom room) {
        FBPlayer value = getGameService().getPlayer().getValue();
        if (value != null && room.getPlayers() != null && room.getPlayers().contains(value.getId())) {
            updateRoom(room);
            return true;
        }
        return false;
    }

    public void createRoom(OnCompletionListener<FbRoom> listener) {
        if (roomLiveData.getValue() != null) {
            return;
        }
        getGameService().createRoom(genRandomName(), new OnCompletionListener<FbRoom>() {
            @Override
            public void onComplete(FbRoom data) {
                updateRoom(data);
                listener.onComplete(data);
            }

            @Override
            public void onFailure(Exception e) {
                listener.onFailure(e);
            }
        });
    }

    public void enterRoom(String roomId, OnCompletionListener<FbRoom> listener) {
        getGameService().enterRoom(roomId, new OnCompletionListener<FbRoom>() {
            @Override
            public void onComplete(FbRoom data) {
                updateRoom(data);
                listener.onComplete(data);
            }

            @Override
            public void onFailure(Exception e) {
                listener.onFailure(e);
            }
        });
    }

    public void addMove(Sign sign, int row, int column, OnCompletionListener<FbRoom> listener) {
        FbRoom room = roomLiveData.getValue();
        if (room == null) {
            return;
        }
        FBMove move = new FBMove();
        move.setRoomId(room.getId());
        move.setX(row);
        move.setY(column);
        move.setXValue(sign == Sign.X);
        getGameService().addMove(move, listener);
    }

    public GameProcess getGameProcess() {
        return gameProcess;
    }

    public Sign handleBoardClicked(int row, int column) {
        return gameProcess.click(row, column);
    }

    public boolean isLastMoveMine(FbRoom room) {
        FBPlayer player = getGameService().getPlayer().getValue();
        if (player != null && room != null) {
            return TextUtils.equals(player.getId(), room.getLastPlayer());
        }
        return false;
    }

    private void clearBoardObserver() {
        if (subscribedLiveData != null) {
            subscribedLiveData.removeObserver(boardObserver);
            subscribedLiveData = null;
        }
    }

    private void updateRoom(FbRoom data) {
        if (roomLiveData.getValue() != null && !TextUtils.equals(roomLiveData.getValue().getId(), data.getId())) {
            clearBoardObserver();
        }
        if (subscribedLiveData == null) {
            subscribedLiveData = getBoard(data);
            subscribedLiveData.observeForever(boardObserver);
        }
        roomLiveData.setValue(data);
    }

    private LiveData<List<FbRoom>> getRooms() {
        return getGameService().getRooms();
    }

    private LiveData<FBBoard> getBoard(FbRoom room) {
        return getGameService().getBoard(room);
    }

    private String genRandomName() {
        return "Room " + (int) (100 * Math.random());
    }

    private void updateBoard(FBBoard board) {
        gameProcess.updateBoard(GameBoard.parse(board.getState()), board.getNextX() ? Sign.X : Sign.O);
    }

    private final Observer<List<FbRoom>> roomsObserver = fbRooms -> {
        FbRoom currentRoom = roomLiveData.getValue();
        if (currentRoom != null && fbRooms != null) {
            fbRooms.stream()
                    .filter(room -> room.getId().equals(currentRoom.getId()))
                    .findAny()
                    .ifPresent(this::updateRoom);
        }
    };

    private final Observer<FBBoard> boardObserver = fbBoard -> {
        updateBoard(fbBoard);
        boardLiveData.setValue(fbBoard);
    };

}
