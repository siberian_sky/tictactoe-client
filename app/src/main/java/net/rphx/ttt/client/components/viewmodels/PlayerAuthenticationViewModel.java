package net.rphx.ttt.client.components.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.appspot.jbtttapp.ttt.model.FBPlayer;

import net.rphx.ttt.client.GameContext;
import net.rphx.ttt.client.TicTacToeApp;
import net.rphx.ttt.client.authentication.AuthInfo;

public class PlayerAuthenticationViewModel extends AndroidViewModel {

    private final GameContext gameContext;

    public PlayerAuthenticationViewModel(@NonNull Application application) {
        super(application);
        gameContext = TicTacToeApp.get(application).getGameContext();
    }

    public void init(AuthInfo authInfo) {
        gameContext.init(authInfo);
    }

    public LiveData<FBPlayer> getPlayer() {
        return gameContext.getGameService().getPlayer();
    }
}
