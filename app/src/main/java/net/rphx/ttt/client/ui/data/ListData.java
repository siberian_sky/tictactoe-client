package net.rphx.ttt.client.ui.data;

public interface ListData<T> {
    T get(int index);

    int size();
}
