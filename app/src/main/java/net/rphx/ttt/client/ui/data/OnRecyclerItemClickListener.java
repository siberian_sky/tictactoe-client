package net.rphx.ttt.client.ui.data;

import android.view.View;

public interface OnRecyclerItemClickListener<I> {

    void onItemClick(View view, int position, BaseAdapter.RecyclerItemHolder<I> viewHolder);
}
