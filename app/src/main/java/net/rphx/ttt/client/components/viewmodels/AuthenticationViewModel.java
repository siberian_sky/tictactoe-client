package net.rphx.ttt.client.components.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import net.rphx.ttt.client.authentication.AuthInfo;
import net.rphx.ttt.client.components.UserRepository;

public class AuthenticationViewModel extends ViewModel {

    private LiveData<AuthInfo> authInfo;

    public void init() {
        authInfo = UserRepository.getInstance().getAuthInfo();
    }

    public LiveData<AuthInfo> getAuthInfo() {
        return authInfo;
    }

}
