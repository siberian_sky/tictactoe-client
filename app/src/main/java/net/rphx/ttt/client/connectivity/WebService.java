package net.rphx.ttt.client.connectivity;

import com.appspot.jbtttapp.ttt.model.FBBoard;
import com.appspot.jbtttapp.ttt.model.FBMove;
import com.appspot.jbtttapp.ttt.model.FBPlayer;
import com.appspot.jbtttapp.ttt.model.FbRoom;

import java.io.IOException;
import java.util.List;

public interface WebService {
    FbRoom createRoom(FbRoom room) throws IOException;

    FbRoom enterRoom(FbRoom room) throws IOException;

    FBPlayer createPlayer(FBPlayer player) throws IOException;

    FBPlayer getPlayer() throws IOException;

    FbRoom addMove(FBMove move) throws IOException;

    FBBoard getBoard(FbRoom room) throws IOException;

    List<FbRoom> getRooms() throws IOException;

    void addListener(WebserviceListener listener);

    void removeListener(WebserviceListener listener);

    void close();
}
